Considerations for the test assignment:

1. I'm not a designer as you could see.
2. Chosed MVVM design pattern with databinding and android architecture.
	- Retrofit 2 and LiveData
	- ViewModel
3. Koin for dependency injection
4. Unit tests are done on junit with Mockito: as example i tested some logic I added to the Car model and the MainActivityViewModel
4. SwipeRefresh to refresh the list of cars.
5. Regarding the adapter i should have been using another viewModel to handle the click event to be stricht to the pattern i chose, but i decided not to do it because the boiler plate was not worth the usage.
6. Reused the rating layout a few times passing parameters as needed.
7. Added different dimension to adapt to different screen resolution.
8. I did not modify the gitIgnore file, so there are committed file that usually are ignored.
