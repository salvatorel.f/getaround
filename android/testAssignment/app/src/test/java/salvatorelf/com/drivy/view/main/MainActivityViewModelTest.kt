package salvatorelf.com.drivy.view.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.times
import junit.framework.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import salvatorelf.com.drivy.model.Car
import salvatorelf.com.drivy.network.NetworkService

class MainActivityViewModelTest {
    @Mock
    lateinit var mockNetworkService: NetworkService
    @Mock
    lateinit var mockThrowable: Throwable
    @Mock
    lateinit var mockApiCall: Call<List<Car>>
    @Mock
    lateinit var mockApiResponse: Response<List<Car>>
    @Mock
    lateinit var mockResponseBody: List<Car>

    @get:Rule
    var rule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    private var argumentCaptorApiCall = argumentCaptor<Callback<List<Car>>>()

    private lateinit var subject: MainActivityViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        subject = MainActivityViewModel(mockNetworkService)
        `when`(mockNetworkService.getCars()).thenReturn(mockApiCall)
        `when`(mockApiResponse.body()).thenReturn(mockResponseBody)

    }

    @Test
    fun `it should set Loading to false and return the proper throwable when an error is returned on getCars()`() {
        subject.getCars()

        verify(mockApiCall).enqueue(argumentCaptorApiCall.capture())
        argumentCaptorApiCall.firstValue.onFailure(mockNetworkService.getCars(), mockThrowable)
        assertEquals(subject.isLoading.get(), false)
        assertNotNull(subject.cars.value?.throwable)
        assertNull(subject.cars.value?.cars)
    }

    @Test
    fun `it should set Loading to false and return the proper response when NO error is returned on getCars()`() {
        subject.getCars()

        verify(mockApiCall).enqueue(argumentCaptorApiCall.capture())
        argumentCaptorApiCall.firstValue.onResponse(mockNetworkService.getCars(), mockApiResponse)
        assertEquals(subject.isLoading.get(), false)
        assertNull(subject.cars.value?.throwable)
        assertNotNull(subject.cars.value?.cars)

    }
    @Test
    fun `it should set Loading to true and fetch the cars on onRefresh()`() {
        subject.onRefresh()
        assertEquals(subject.isLoading.get(), true)
        verify(mockApiCall, times(1)).enqueue(argumentCaptorApiCall.capture())
    }
}