package salvatorelf.com.drivy.model

import org.junit.Assert
import org.junit.Before
import org.junit.Test
import salvatorelf.com.drivy.BuildConfig


class CarTest {
    private lateinit var subject: Car

    @Before
    fun setUp() {
        subject = Car(
            brand = "Citroen",
            model = "C3",
            pictureUrl = "https://raw.githubusercontent.com/drivy/jobs/master/android/api/pictures/13.jpg",
            pricePerDay = 17,
            rating = Rating(average = 4.697711, count = 259),
            owner = Owner(
                name = "Elmira Sorrell",
                pictureUrl = "https://drivy-assets.imgix.net/jobs/team/howard.jpg",
                rating = Rating(average = 4.318206, count = 255)
            )
        )
    }


    @Test
    internal fun `it should return EUR for this configuration`() {
        Assert.assertEquals(
            "EUR", BuildConfig.CURRENCY_CODE
        )
    }

    @Test
    internal fun `it should return proper formatted currency on getPriceCurrency()`() {
        Assert.assertEquals(
            BuildConfig.CURRENCY_CODE+17, subject.getPriceCurrency()
        )
    }
}