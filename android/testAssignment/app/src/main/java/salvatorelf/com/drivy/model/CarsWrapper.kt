package salvatorelf.com.drivy.model

class CarsWrapper {
    var cars: List<Car>? = null
    var throwable: Throwable? = null

    constructor(cars: List<Car>) {
        this.cars = cars
    }

    constructor(throwable: Throwable) {
        this.throwable = throwable
    }

}