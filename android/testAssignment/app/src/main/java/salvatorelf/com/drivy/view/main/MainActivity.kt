package salvatorelf.com.drivy.view.main

import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.appbar.view.*
import org.koin.android.ext.android.inject
import salvatorelf.com.drivy.R
import salvatorelf.com.drivy.databinding.ActivityMainBinding
import salvatorelf.com.drivy.model.Car
import salvatorelf.com.drivy.model.CarsWrapper
import salvatorelf.com.drivy.network.NetworkService
import salvatorelf.com.drivy.view.BaseActivity
import salvatorelf.com.drivy.view.detail.DetailActivity
import salvatorelf.com.drivy.view.detail.DetailActivity.Companion.CAR


class MainActivity : BaseActivity(), CarItemCallback {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainActivityViewModel
    private lateinit var recyclerviewAdapter: CarsAdapter

    private val networkService: NetworkService by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setSupportActionBar(binding.toolbarLayout.toolbar)
        viewModel = ViewModelProviders.of(
            this, MainActivityViewModelFactory(networkService)
        ).get(MainActivityViewModel::class.java)

        binding.viewModel = viewModel
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerviewAdapter = CarsAdapter(this)
        binding.recyclerView.adapter = recyclerviewAdapter

        viewModel.cars.observe(this, object : Observer<CarsWrapper> {
            override fun onChanged(t: CarsWrapper?) {
                if (t?.throwable == null && !t?.cars.isNullOrEmpty()) {
                    recyclerviewAdapter.setData(t?.cars!!)
                } else {
                    showErrorDialog(getString(R.string.error_dialog_title), getString(R.string.error_dialog_message))
                }
            }
        })
        if (savedInstanceState == null) {
            viewModel.onRefresh()
        }
    }

    override fun onCarClicked(car: Car) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra(CAR, car)

        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
    }
}
