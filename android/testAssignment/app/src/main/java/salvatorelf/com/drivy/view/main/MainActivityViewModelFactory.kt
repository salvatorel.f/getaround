package salvatorelf.com.drivy.view.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import salvatorelf.com.drivy.network.NetworkService

class MainActivityViewModelFactory(
    private val networkService: NetworkService
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainActivityViewModel(networkService) as T
    }
}