package salvatorelf.com.drivy.model

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@SuppressLint("ParcelCreator")
//Bug on parcelize https://youtrack.jetbrains.com/issue/KT-19300
data class Owner(
    val name: String? = "", @SerializedName("picture_url") val pictureUrl: String? = "",
    val rating: Rating? = null
) : Parcelable