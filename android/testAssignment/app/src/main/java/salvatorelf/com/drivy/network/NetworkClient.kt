package salvatorelf.com.drivy.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import salvatorelf.com.drivy.BuildConfig


class NetworkClient {
    companion object {
        fun create(): NetworkService {
            val retrofit = Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create(NetworkService::class.java)
        }
    }
}